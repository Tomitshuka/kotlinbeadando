package com.example.autokolcsonzo.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "car_table")
data class Car(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val brand: String,
    val model: String,
    val rentCost: Int,
    val productionYear: Int,
    val seats: Int,
    val available: Boolean
): Parcelable