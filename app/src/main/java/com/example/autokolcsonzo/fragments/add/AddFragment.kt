package com.example.autokolcsonzo.fragments.add

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.autokolcsonzo.R
import com.example.autokolcsonzo.model.Car
import com.example.autokolcsonzo.viewmodel.CarViewModel
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AddFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var mCarViewModel: CarViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add, container, false)

        mCarViewModel = ViewModelProvider(this).get(CarViewModel::class.java)

        view.add_btn.setOnClickListener{
            insertDataToDatabase()
        }

        return view
    }

    private fun insertDataToDatabase() {
        val brand = addBrand_et.text.toString()
        val model = addModel_et.text.toString()
        val rent_cost = addRentCost.text
        val production_year = addProductionYear.text
        val seats = addSeats.text

        if(inputCheck(brand, model, rent_cost, production_year, seats))
        {
            val car = Car(
                0,
                brand,
                model,
                Integer.parseInt(rent_cost.toString()),
                Integer.parseInt(production_year.toString()),
                Integer.parseInt(seats.toString()),
       true
            )

            mCarViewModel.addCar(car)
            Toast.makeText(requireContext(), "Autó hozzáadva!", Toast.LENGTH_LONG).show()

            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        }
        else
        {
            Toast.makeText(requireContext(), "Minden mező kitöltése kötelező!", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(brand: String, model: String, rent_cost: Editable, productionYear: Editable, seats: Editable): Boolean
    {
        if(TextUtils.isEmpty(brand))
            return false
        if(TextUtils.isEmpty(model))
            return false
        if(rent_cost.isEmpty())
            return false
        if(productionYear.isEmpty())
            return false
        if(seats.isEmpty())
            return false
        return true
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AddFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}