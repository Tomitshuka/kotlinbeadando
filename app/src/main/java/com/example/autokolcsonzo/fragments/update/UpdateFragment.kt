package com.example.autokolcsonzo.fragments.update

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.autokolcsonzo.R
import com.example.autokolcsonzo.model.Car
import com.example.autokolcsonzo.viewmodel.CarViewModel
import kotlinx.android.synthetic.main.fragment_update.*
import kotlinx.android.synthetic.main.fragment_update.view.*

class UpdateFragment : Fragment() {

    private val args by navArgs<UpdateFragmentArgs>()

    private lateinit var mCarViewModel: CarViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_update, container, false)

        mCarViewModel = ViewModelProvider(this).get(CarViewModel::class.java)

        view.updateBrand.setText(args.currentCar.brand)
        view.updateModel.setText(args.currentCar.model)
        view.updateRentCost.setText(args.currentCar.rentCost.toString())
        view.updateProductionYear.setText(args.currentCar.productionYear.toString())
        view.updateSeats.setText(args.currentCar.seats.toString())

        view.update_btn.setOnClickListener()
        {
            updateItem()
        }

        view.rent.setOnClickListener()
        {
            val action = UpdateFragmentDirections.actionUpdateFragmentToRent(args.currentCar)
            findNavController().navigate(action)
        }

        setHasOptionsMenu(true)

        return view
    }

    private fun updateItem()
    {
        // TODO: Check int vars before init, cause it will crash the app!
        val brand = updateBrand.text.toString()
        val model = updateModel.text.toString()
        val rent_cost = Integer.parseInt(updateRentCost.text.toString())
        val production_year = Integer.parseInt(updateProductionYear.text.toString())
        val seats = Integer.parseInt(updateSeats.text.toString())

        if(inputCheck(brand, model, updateRentCost.text, updateProductionYear.text, updateSeats.text))
        {
            val updateCar = Car(args.currentCar.id, brand, model, rent_cost, production_year, seats, true)

            mCarViewModel.updateCar(updateCar)

            Toast.makeText(requireContext(), "Módosítás sikeres!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }
        else
        {
            Toast.makeText(requireContext(), "Minden mező kitöltése kötelező!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(brand: String, model: String, rent_cost: Editable, productionYear: Editable, seats: Editable): Boolean
    {
        if(TextUtils.isEmpty(brand))
            return false
        if(TextUtils.isEmpty(model))
            return false
        if(rent_cost.isEmpty())
            return false
        if(productionYear.isEmpty())
            return false
        if(seats.isEmpty())
            return false
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menu_delete)
        {
            deleteUser()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteUser()
    {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Igen"){_,_->
            mCarViewModel.deleteCar(args.currentCar)
            Toast.makeText(requireContext(), "Autó sikeresen törölve!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }
        builder.setNegativeButton("Nem"){_,_->

        }
        builder.setMessage("Biztosan törölni akarod?")
        builder.create().show()
    }

}