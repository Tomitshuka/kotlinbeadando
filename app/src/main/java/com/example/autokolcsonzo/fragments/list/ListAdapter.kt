package com.example.autokolcsonzo.fragments.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.autokolcsonzo.R
import com.example.autokolcsonzo.model.Car
import kotlinx.android.synthetic.main.custom_row.view.*
import java.text.NumberFormat
import java.util.*

class ListAdapter() : RecyclerView.Adapter<ListAdapter.MyViewHolder>()
{
    private var carList = ArrayList<Car>()

    class MyViewHolder (itemView: View): RecyclerView.ViewHolder(itemView)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_row, parent, false))
    }

    override fun getItemCount(): Int {
        return carList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = carList[position]
        holder.itemView.txt.text = currentItem.id.toString()
        holder.itemView.Brand_text.text = currentItem.brand
        holder.itemView.Model_text.text = currentItem.model

        val format  = NumberFormat.getNumberInstance(Locale.UK)
        format.maximumFractionDigits = 0
        holder.itemView.RentCost_text.text = format.format(Integer.parseInt(currentItem.rentCost.toString())) + " Ft / nap"

        holder.itemView.custom_row.setOnClickListener{
            val action = ListFragmentDirections.actionListFragmentToUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    fun setData(car: List<Car>)
    {
        carList.clear()
        carList.addAll(car);
        this.notifyDataSetChanged()
    }

}