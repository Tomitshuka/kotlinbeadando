package com.example.autokolcsonzo.fragments

import android.app.DatePickerDialog
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.autokolcsonzo.R
import com.example.autokolcsonzo.fragments.update.UpdateFragmentArgs
import com.example.autokolcsonzo.model.Car
import com.example.autokolcsonzo.viewmodel.CarViewModel
import kotlinx.android.synthetic.main.fragment_rent.*
import kotlinx.android.synthetic.main.fragment_rent.view.*
import kotlinx.android.synthetic.main.fragment_update.view.*
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [rent.newInstance] factory method to
 * create an instance of this fragment.
 */
class rent : Fragment() {

    private var param1: String? = null
    private var param2: String? = null

    private val args by navArgs<UpdateFragmentArgs>()

    private lateinit var mCarViewModel: CarViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        val view = inflater.inflate(R.layout.fragment_rent, container, false)

        mCarViewModel = ViewModelProvider(this).get(CarViewModel::class.java)

        //view.finalRentCost.setText(args.currentCar.rentCost.toString())

        view.renterBirthDate.setOnClickListener{funDateRenterBirth(view)}
        view.rentStart.setOnClickListener{funDateRentStart(view)}
        view.rentEnd.setOnClickListener{funDateRentEnd(view)}

        view.rentEnd.addTextChangedListener{ updateFinalRentPrice() }
        view.rentStart.addTextChangedListener{ updateFinalRentPrice() }

        view.submitRentBtn.setOnClickListener()
        {
            // Check if everything is correct
            if(rentingCheck(renter_name.text.toString(), renterBirthDate.text.toString(), rentStart.text.toString(), rentEnd.text.toString()))
            {
                val updateCar = Car(args.currentCar.id, args.currentCar.brand, args.currentCar.model,
                        args.currentCar.rentCost, args.currentCar.productionYear, args.currentCar.seats, false) // Change available to false
                mCarViewModel.updateCar(updateCar)

                Toast.makeText(requireContext(), "Fogalalás sikeres!", Toast.LENGTH_LONG).show()
                findNavController().navigate(R.id.action_rent_to_listFragment)
            }
            else
            {
                Toast.makeText(requireContext(), "Minden mező kitöltése kötelező!", Toast.LENGTH_LONG).show()
            }
        }

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment rent.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            rent().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun rentingCheck(renterName: String, birthdate: String, rentStart: String, rentEnd: String): Boolean
    {
        if(TextUtils.isEmpty(renterName))
            return false
        if(TextUtils.isEmpty(birthdate))
            return false
        if(TextUtils.isEmpty(rentStart))
            return false
        if(TextUtils.isEmpty(rentEnd))
            return false
        return true
    }

    private fun funDateRenterBirth(view: View)
    {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)

        val dpd = DatePickerDialog(view.context, android.R.style.Theme_Material_Light_Dialog, DatePickerDialog.OnDateSetListener { datePicker, year, monthOfYear, dayOfMonth ->
            renterBirthDate.setText("" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth)
        }, year, month, day)
        dpd.show()
    }

    private fun funDateRentStart(view: View)
    {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)

        val dpd = DatePickerDialog(view.context, android.R.style.Theme_Material_Light_Dialog, DatePickerDialog.OnDateSetListener { datePicker, year, monthOfYear, dayOfMonth ->
            rentStart.setText("" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth)
        }, year, month, day)
        dpd.show()
        //updateFinalRentPrice()
    }

    private fun funDateRentEnd(view: View)
    {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)

        val dpd = DatePickerDialog(view.context, android.R.style.Theme_Material_Light_Dialog, DatePickerDialog.OnDateSetListener { datePicker, year, monthOfYear, dayOfMonth ->
            rentEnd.setText("" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth)
        }, year, month, day)
        dpd.show()
        //updateFinalRentPrice()
    }

    private fun updateFinalRentPrice()
    {
        if(rentEnd.text.isNotEmpty() && rentStart.text.isNotEmpty())
        {
            val start = SimpleDateFormat("yyyy-MM-dd").parse(rentStart.text.toString())
            val end = SimpleDateFormat("yyyy-MM-dd").parse(rentEnd.text.toString())

            val diff = end.time - start.time
            val days = java.util.concurrent.TimeUnit.DAYS.convert(diff, java.util.concurrent.TimeUnit.MILLISECONDS)

            finalRentCost.setText((days * args.currentCar.rentCost).toString() + " Ft")
        }
    }

}