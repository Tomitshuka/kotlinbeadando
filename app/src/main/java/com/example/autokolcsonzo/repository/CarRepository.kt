package com.example.autokolcsonzo.repository

import androidx.lifecycle.LiveData
import com.example.autokolcsonzo.data.CarDao
import com.example.autokolcsonzo.model.Car
import kotlinx.coroutines.flow.Flow

class CarRepository(private val carDao: CarDao) {

    val readAllData: LiveData<List<Car>> = carDao.readAllData()

    suspend fun addCar(car: Car)
    {
        carDao.addCar(car)
    }

    suspend fun updateCar(car: Car)
    {
        carDao.updateCar(car)
    }

    suspend fun deleteCar(car: Car)
    {
        carDao.deleteCar(car)
    }

    suspend fun deleteAllCars()
    {
        carDao.deleteAllCars()
    }

    fun searchDatabase(searchQuery: String): Flow<List<Car>> {
        return carDao.searchDatabase(searchQuery)
    }
}