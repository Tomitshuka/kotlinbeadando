package com.example.autokolcsonzo.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.autokolcsonzo.model.Car
import kotlinx.coroutines.flow.Flow

@Dao
interface CarDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addCar(car: Car)

    // Gives back the available cars
    @Query(value = "SELECT * FROM car_table WHERE available = 1 ORDER BY id ASC")
    fun readAllData(): LiveData<List<Car>>

    // Gives back the all car
    //@Query(value = "SELECT * FROM car_table ORDER BY id ASC")
    //fun readAllData(): LiveData<List<Car>>

    @Delete
    suspend fun deleteCar(car: Car)

    @Query("DELETE FROM car_table")
    suspend fun deleteAllCars()

    @Update
    suspend fun updateCar(car: Car)

    @Query("SELECT * FROM car_table WHERE brand LIKE :searchQuery OR model LIKE :searchQuery OR rentCost LIKE :searchQuery")
    fun searchDatabase(searchQuery: String): Flow<List<Car>>

}