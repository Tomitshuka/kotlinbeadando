package com.example.autokolcsonzo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.autokolcsonzo.data.CarDatabase
import com.example.autokolcsonzo.repository.CarRepository
import com.example.autokolcsonzo.model.Car
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CarViewModel (application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<Car>>
    val repository: CarRepository

    init
    {
        val carDao = CarDatabase.getDatabase(
            application
        ).carDao()
        repository = CarRepository(carDao)
        readAllData = repository.readAllData
    }

    fun addCar(car: Car)
    {
        viewModelScope.launch(Dispatchers.IO)
        {
            repository.addCar(car)
        }
    }

    fun updateCar(car: Car)
    {
        viewModelScope.launch(Dispatchers.IO)
        {
            repository.updateCar(car)
        }
    }

    fun deleteCar(car: Car)
    {
        viewModelScope.launch(Dispatchers.IO)
        {
            repository.deleteCar(car)
        }
    }

    fun deleteAllCars()
    {
        viewModelScope.launch(Dispatchers.IO)
        {
            repository.deleteAllCars()
        }
    }

    fun searchDatabase(searchQuery: String): LiveData<List<Car>> {
        return repository.searchDatabase(searchQuery).asLiveData()
    }

}