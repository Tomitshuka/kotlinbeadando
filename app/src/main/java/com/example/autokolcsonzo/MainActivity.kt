package com.example.autokolcsonzo

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.ImageLoader
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.example.autokolcsonzo.fragments.list.ListAdapter
import com.example.autokolcsonzo.model.Car
import com.example.autokolcsonzo.viewmodel.CarViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.fragment_list.view.*

class MainActivity : AppCompatActivity() {

    private val adapter: ListAdapter by lazy { ListAdapter() }
    private lateinit var mViewModel: CarViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mViewModel = ViewModelProvider(this).get(CarViewModel::class.java)

        //setContentView(R.layout.activity_main)

        mViewModel.readAllData.observe(this) {
            adapter.setData(it)
        }

        mViewModel.repository.readAllData.observe(this)
        {
            adapter.setData(it)
        }

        setupActionBarWithNavController(findNavController(R.id.fragment))

        this.fragment.recyclerView
        //setContentView(R.layout.fragment_list)
        //recyclerView.adapter = adapter
        //recyclerView.layoutManager = LinearLayoutManager(this)
        //setContentView(R.layout.activity_main)
    }

    private suspend fun getBitmap(): Bitmap {
        val loading = ImageLoader(this)
        val request = ImageRequest.Builder(this)
            .data("https://avatars3.githubusercontent.com/u/14994036?s=400&u=2832879700f03d4b37ae1c09645352a352b9d2d0&v=4")
            .build()

        val result = (loading.execute(request) as SuccessResult).drawable
        return (result as BitmapDrawable).bitmap
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}