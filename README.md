﻿# Car rental application

This repository contains a Kotlin application which is a car rental shop. It's written in kotlin and used Android Studio for implementation.

# Main constituent elements
Mainly used:

- Room database
- One activity + fragments
- [Repository pattern](https://deviq.com/design-patterns/repository-pattern)
- [CRUD functions](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)

# Fragments 

## Main:

![Main](main.png "Main fragment")

## Update:

![Update](update.png "Update fragment")

## Add:

![Add](add.png "Add fragment")

## Rent:

![Rent](rent.png?raw=true "Rent fragment")




